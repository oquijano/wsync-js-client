import {ServerDate,DEFAULT_SYNC_SERVER} from './wsync.js';

// Event when a countdown is over

// it bubbles so actions can be easily defined when it is dispatched
var countdownOver = new CustomEvent("countdown-over",{bubbles: true});

export class UndefinedCountdownDeadline extends Error {
    constructor(){
	super("Attribute data-wsync-countdown-message not found.");
	this.name = "UndefinedCountdownDeadline";
    }
}


export class CountdownDeadline{
    
    constructor(sdate,end_date){
	this.sdate=sdate;
	this.end_date=end_date;
    }

    time_remaining(){
	var remaining_time = this.end_date - this.sdate.get();
	if (remaining_time <= 0){	    
	    return 0;
	} else {
	    return remaining_time;
	}	
    }
}

export class CountdownDeadlineElem{
    
    constructor(elem,sdate=new ServerDate(),update_interval=50){
	// sdate should be a ServerDate object	
	// elem should be an html element whose inner Text is the countdown	
	// update_interval number of milliseconds to update the countdown
	if (! elem.hasAttribute("data-wsync-countdown-end")){
	    throw new UndefinedCountdownDeadline();
	}
	
	this.elem=elem;
	this.update_interval=update_interval;
	this.define_over_message();
	this.end_date=Date.parse(elem.dataset.wsyncCountdownEnd);
	this.countdown= new CountdownDeadline(sdate,this.end_date);
	this.start();
    }

    define_over_message(){
	// if the attribute data-wsync-countdown-over-message is defined
	// then its value is shown when the countdown is over.
	var message_attribute_name="data-wsync-countdown-message";
	if ( this.elem.hasAttribute(message_attribute_name) ){
	    this.elem.addEventListener("countdown-over",
				       (event) =>{
					   this.elem.innerHTML = this.elem.getAttribute(message_attribute_name);
				       })
	}
	
    }

    start(){
	this.update_interval=setInterval(self=>{self.show_remaining_time(self)},this.update_interval,this);
    }

    stop(){
	clearInterval(this.update_interval);
    }
    

    show_remaining_time(self){
	var time_left=self.countdown.time_remaining();
	self.elem.innerHTML=self.format_time(Math.floor( time_left / 1000 ));
	if( time_left <= 0 ){
	    self.elem.dispatchEvent(countdownOver);
	    self.stop();
	}
    }
    
    format_time(seconds){
	// ~~ = Math.floor()
	var remaining_seconds=seconds;
	var seconds_in_day = 3600 * 24;
	var days = ~~(seconds / seconds_in_day);
	remaining_seconds -= days*seconds_in_day;
	var hrs = ~~( remaining_seconds   / 3600);
	remaining_seconds -=  hrs*3600;
	var mins = ~~(remaining_seconds / 60);
	remaining_seconds -= mins*60;
	var secs = ~~seconds % 60;
	var ret = "";
	if (days > 0){
	    ret += "" + days + " ";
	}
	if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
	}
	ret += "" + mins + ":" + (secs < 10 ? "0" : "");
	ret += "" + secs;
	return ret;
    }
}

function get_attribute_value(attribute,default_value=null){
    var elem = document.querySelector(`[${attribute}]`);
    if(elem != null){
	return elem.getAttribute(attribute);
    }else{
	return default_value;
    }
}


export function start_all_countdowns(){
    
    var update_interval=get_attribute_value("data-wsync-server-date-update-interval");
    var sdate_url=get_attribute_value("data-wsync-server-date-url",DEFAULT_SYNC_SERVER);

    if(update_interval!=null){
	update_interval=~~update_interval;
    }
    
    var sdate=new ServerDate(update_interval,sdate_url);
    
    var all_countdown_elems = document.querySelectorAll("[data-wsync-countdown-end]");
    var all_countdowns = Array.from(all_countdown_elems).map( x=> {
	return new CountdownDeadlineElem(x,sdate)
    })
    return all_countdowns;
}
