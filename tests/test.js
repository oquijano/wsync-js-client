import {ServerDate,UpdateIntervalError} from "../wsync.js";
import {CountdownDeadlineElem,UndefinedCountdownDeadline} from "../countdown.js";

var sdate_fail=new ServerDate(null,"http://example.com");
var sdate=new ServerDate();



var hola = await sdate.sync_promise;

sdate_fail.sync_promise.catch(()=>{
    sdate_fail.socket.close();
    var info = document.querySelector("#info");
    info.innerHTML=`Connected<br/>Offset: ${sdate.offset}`;
    run_tests();
    mocha.run();
});




function run_tests(){
    
    describe("Connection Promise",function(){

	it("Check the connection works",function(){
	    assert.equal(sdate.socket.readyState,1);
	})

	it("Check the connection doesn't work",function(){
	    assert.equal(sdate_fail.offset,null)
	})

	it("Error is thrown with wrong update_interval",function(){
	    assert.throws(()=>{new ServerDate(-5,null)},UpdateIntervalError);
	})
	
    })

    describe("Countdown checks",function(){
	it("Check error is thrown with incorrect countdown elements",
	   function(){
	       var error_countdown=document.querySelector("#error-countdown");
	       assert.throws(()=>{
		   var x=new CountdownDeadlineElem(error_countdown,sdate);
	       },UndefinedCountdownDeadline)
	   })
    })
    
}







