import {ReconnectingWebSocket} from './reconnecting-websocket.js';

export var DEFAULT_SYNC_SERVER="wss://wsync.net";

export class UpdateIntervalError extends Error {
    constructor(){
	super("update_interval must either be null or a positive integer.");
	this.name="UpdateIntervalError";
    }
}

function is_positive_integer(n){
    return (Number.isInteger(n) && n>0);
}


export class ServerDate{
    
    constructor(update_interval=null,ws_path=DEFAULT_SYNC_SERVER){
	
	this.update_interval = update_interval;
	this.offset = null;
	this.precission =  Infinity ;
	this.socket = new ReconnectingWebSocket(ws_path);


	let self=this;
	
	this.sync_promise=new Promise(function(resolve,reject){
	    self.resolve_sync_promise=resolve;
	    setTimeout(()=>{reject(true)},5000);
	});

	this.socket.onmessage = (e) => {
	    let current_date = new Date().getTime();
	    let dates = JSON.parse(e.data);
	    let current_precission = current_date - dates[0];
	    if(current_precission < this.precission){

		this.offset = (current_date + dates[0])/2 - dates[1];
		
		if( this.precission == Infinity ){
		    this.resolve_sync_promise(this.offset);
		}
		
		this.precission = current_precission;

	    }
	}


	this.socket.onopen = (e) => {
	    this.offset_update();
	}

	if(update_interval != null){
	    if(is_positive_integer(update_interval)){
		setInterval(()=>{this.offset_update()},
			    update_interval*60*1000);
	    }else{
		this.socket.close();
		throw new UpdateIntervalError;
	    }
	}else{
	    // When update interval is null close the socket after one
	    // minute.
	    setTimeout(()=>{this.socket.close();},60000);
	}
	
    }

    offset_update(){
	for (let i=0;i<10;i++){
	    setTimeout(() => {this.socket.send(JSON.stringify(new Date().getTime()))},
		       i*20);
	}
    }

    get(){
	return new Date().getTime() - this.offset;
    }
    
}
